```
pip install -r requirements.txt
python3 download.py snes
```

````
snes                = "https://the-eye.eu/public/rom/SNES/"
gameboyadvanced     = "https://the-eye.eu/public/rom/Nintendo%20Gameboy%20Advance/"
gameboycolor        = "https://the-eye.eu/public/rom/Nintendo%20Gameboy%20Color/"
gameboy             = "https://the-eye.eu/public/rom/Nintendo%20Gameboy/"
nes                 = "https://the-eye.eu/public/rom/NES/"
genesis             = "https://the-eye.eu/public/rom/Sega%20Genesis/"
playstationntsc     = "https://the-eye.eu/public/rom/Playstation/Games/NTSC/"
playstationntscj    = "https://the-eye.eu/public/rom/Playstation/Games/NTSC-J/"
playstationpal      = "https://the-eye.eu/public/rom/Playstation/Games/PAL/"
nintendo64          = "https://the-eye.eu/public/rom/Nintendo%2064/Roms/"
pcengine            = "https://the-eye.eu/public/rom/NEC%20PC%20Engine%20TurboGrafx%2016/"
neogeo              = "https://the-eye.eu/public/rom/SNK%20Neo%20Geo/"
segamaster          = "https://the-eye.eu/public/rom/Sega%20Master%20System/"
dreamcast           = "https://the-eye.eu/public/rom/Sega%20Dreamcast/"
commodore           = "https://the-eye.eu/public/rom/Commodore%2064/"
````