import requests
import sys
import shutil
import os
from bs4 import BeautifulSoup

baselink = sys.argv[1]

snes                = "https://the-eye.eu/public/rom/SNES/"
gameboyadvanced     = "https://the-eye.eu/public/rom/Nintendo%20Gameboy%20Advance/"
gameboycolor        = "https://the-eye.eu/public/rom/Nintendo%20Gameboy%20Color/"
gameboy             = "https://the-eye.eu/public/rom/Nintendo%20Gameboy/"
nes                 = "https://the-eye.eu/public/rom/NES/"
genesis             = "https://the-eye.eu/public/rom/Sega%20Genesis/"
playstationntsc     = "https://the-eye.eu/public/rom/Playstation/Games/NTSC/"
playstationntscj    = "https://the-eye.eu/public/rom/Playstation/Games/NTSC-J/"
playstationpal      = "https://the-eye.eu/public/rom/Playstation/Games/PAL/"
nintendo64          = "https://the-eye.eu/public/rom/Nintendo%2064/Roms/"
pcengine            = "https://the-eye.eu/public/rom/NEC%20PC%20Engine%20TurboGrafx%2016/"
neogeo              = "https://the-eye.eu/public/rom/SNK%20Neo%20Geo/"
segamaster          = "https://the-eye.eu/public/rom/Sega%20Master%20System/"
dreamcast           = "https://the-eye.eu/public/rom/Sega%20Dreamcast/"
commodore           = "https://the-eye.eu/public/rom/Commodore%2064/"

def move_dir(src: str, dst: str, pattern: str = '*'):
    if not os.path.isdir(dst):
        pathlib.Path(dst).mkdir(parents=True, exist_ok=True)
    for f in fnmatch.filter(os.listdir(src), pattern):
        shutil.move(os.path.join(src, f), os.path.join(dst, f))

def switch(i):
    switcher = {
        'snes': snes,
        'gameboyadvanced': gameboyadvanced,
        'gameboycolor': gameboycolor,
        'gameboy': gameboy,
        'nes': nes,
        'genesis': genesis,
        'playstationntsc': playstationntsc,
        'playstationntscj': playstationntscj,
        'playstationpal': playstationpal,
        'nintendo64': nintendo64,
        'pcengine': pcengine,
        'neogeo': neogeo,
        'segamaster': segamaster,
        'dreamcast': dreamcast,
        'commodore': commodore
    }
    return switcher.get(i, "Invalid selection")

print("downloading from {}".format(switch(baselink)))

base = switch(baselink)
links = requests.get(base)
soup = BeautifulSoup(links.text, "html.parser")
link_container = soup.find("pre").find_all('a')

if not os.path.exists(baselink):
    os.makedirs(baselink)

for link in link_container:
    try:
        filename = link.get('href')
        download_link = "{}{}".format(base, link.get('href'))
        print("downloading {}".format(download_link))
        r = requests.get(str(download_link), allow_redirects=True)
        open("{}/".format(baselink) + filename, 'wb').write(r.content)

    except:
        print("not a valid link")